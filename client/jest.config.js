module.exports = {
  collectCoverage: false,
  collectCoverageFrom: ['src/**/*.{js,jsx}'],
  moduleDirectories: ['node_modules', 'src'],
  transformIgnorePatterns: ["node_modules/(?!@toolz/allow)/"],
  moduleNameMapper: {
    "\\.(css|scss|png)$": "<rootDir>/__mocks__/styleMock.js"
  },
  coverageDirectory: 'coverage',
  testEnvironment: 'jsdom',
}