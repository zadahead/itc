
import './App.css';
import { Header } from './Shared/Header';
import { JobSeek } from './Views/JobSeek';
import { Route, Routes } from 'react-router-dom';
import { Home } from './Views/Home';
import { Grid } from 'UIKit';
import { Docs } from './Views/Docs';
import { Doc } from './Views/Doc';

export const App = () => {


    return (
        <>
            <Grid layout="main">
                <div>
                    <Header />
                </div>
                <Routes>
                    <Route path="/seek" element={<JobSeek />} />
                    <Route path="/docs/*" element={<Docs />} />
                    <Route path="/docs/:field/:program/:subject" element={<Doc />} />
                    <Route path="/docs/:field/:program/:subject/:selected" element={<Doc />} />
                    <Route path="*" element={<Home />} />
                </Routes>
            </Grid>
        </>
    )
}