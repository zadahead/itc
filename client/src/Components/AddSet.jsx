import { v4 as uuid } from 'uuid';
import { useDispatch, useSelector } from 'react-redux';
import { addJob, newJob, setJob } from '../State/jobs';
import { addRequirements } from '../State/requirements';
import { Btn, Icon, Input, Between, End, Line, Rows } from 'UIKit';
import { Setter } from '../Components/Setter';
import { addResponsibilities } from '../State/responsibilities';

export const AddSet = () => {
    const dispatch = useDispatch();
    const [job] = useSelector(state => [state.job]);

    const jobInput = {
        p: 'Job Title',
        value: job.title || '',
        onChange: (title) => {
            job.title = title;
            handleSetJob();
        }
    };

    const handleAdd = () => {
        dispatch(addJob({
            id: uuid(),
            ...job
        }))
        handleNewJob();
    }

    const handleNewJob = () => {
        dispatch(newJob());
    }

    const handleSetJob = () => {
        dispatch(setJob(job));
    }



    return (
        <>
            <Rows>
                <h1>ITC - Job Seek</h1>
                <Between>
                    <Input {...jobInput} />
                    <Btn onClick={handleNewJob}>Add New</Btn>
                </Between>
                <Setter
                    title="Requirements"
                    selector={state => state.requirements}
                    dispatcher={addRequirements}
                    selected={job.requirements}
                    onChange={(requirements) => {
                        job.requirements = requirements;
                        handleSetJob();
                    }}
                />

                <Setter
                    title="Responsibilities"
                    selector={state => state.responsibilities}
                    dispatcher={addResponsibilities}
                    selected={job.responsibilities}
                    onChange={(responsibilities) => {
                        job.responsibilities = responsibilities;
                        handleSetJob();
                    }}
                />

                <End>
                    {job.id && (
                        <Line>
                            <Icon i="delete" />
                            <Btn onClick={handleSetJob}>Save</Btn>
                        </Line>
                    )}
                    {!job.id && (
                        <Line>
                            <Btn onClick={handleAdd}>Add</Btn>
                        </Line>
                    )}
                </End>
            </Rows>
        </>
    )
}