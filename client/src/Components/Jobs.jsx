import { useDispatch, useSelector } from "react-redux";
import { selectJob } from "../State/jobs";
import { ItemSelect, Container, Rows } from "UIKit"

export const Jobs = () => {
    const dispatch = useDispatch();

    const [job, jobs] = useSelector(state => [state.job, state.jobs]);
    const selectedJobId = job.id;

    const handleSelect = (jobId) => {
        dispatch(selectJob(jobId))
    }

    return (
        <Rows>
            <Container title="Jobs">
                {jobs && jobs.map(job => (
                    <ItemSelect
                        key={job.id}
                        onClick={() => handleSelect(job.id)}
                        selected={job.id === selectedJobId}
                    >
                        {job.title}
                    </ItemSelect>
                ))}
            </Container>
        </Rows>
    )
}