import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { v4 as uuid } from 'uuid';
import { useInput } from "../Hooks/useInput";
import { Btn, CheckBtn, Input, Container, Line } from "UIKit";

export const Setter = ({ title, dispatcher, selector, onChange, selected }) => {
    const dispatch = useDispatch();
    const list = useSelector(selector);
    const inpt = useInput('', `Add ${title}`);
    const selectedSet = new Set(selected);

    const handleSetReq = (id) => {
        if (selectedSet.has(id)) {
            selectedSet.delete(id);
        } else {
            selectedSet.add(id);
        }
        onChange([...selectedSet]);
    }

    const handleAdd = () => {
        if (!inpt.value) { return }

        dispatch(dispatcher({
            id: uuid(),
            title: inpt.value
        }))
        inpt.empty();
    }

    return (
        <Container title={title}>
            <Line>
                {list && list.map(({ id, title }) => (
                    <CheckBtn key={id} selected={selectedSet.has(id)} onClick={() => handleSetReq(id)}>{title}</CheckBtn>
                ))}
            </Line>
            <Line>
                <Input {...inpt} />
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </Container>
    )
}