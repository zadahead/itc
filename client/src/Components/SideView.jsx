import { useEffect, useRef, useState } from "react";
import { Between, Box, Icon, IconBtn, LinkBtn, Margin, Rows, RowsShort } from "UIKit"

export const SideView = ({ title, list = [], selected, onSort, onAdd, onSelect, onDelete }) => {
    const [onDrag, setOnDrag] = useState('');

    const timerRef = useRef();
    const sourceRef = useRef();
    const wrapRef = useRef();

    useEffect(() => {

        wrapRef.current.addEventListener('mousedown', handleDown);
        wrapRef.current.addEventListener('mouseup', handleUp);

        return () => {
            if (wrapRef?.current) {
                wrapRef.current.removeEventListener('mousedown', handleDown);
                wrapRef.current.removeEventListener('mouseup', handleUp);
            }
        }

    }, [list])

    const handleDown = (e) => {
        const item = e.target.closest('.item');
        if (item) {
            timerRef.current = setTimeout(() => {
                sourceRef.current = item.getAttribute('data-id');
                setOnDrag(true);
            }, 300);
        }
    }

    const handleUp = (e) => {
        clearTimeout(timerRef.current);

        if (sourceRef.current) {
            const item = e.target.closest('.item');
            if (item) {
                const source = sourceRef.current;
                const target = item.getAttribute('data-id');
                handleChange(source, target);
            }
            setOnDrag(false);
            sourceRef.current = null;
        }
    }

    const handleChange = (source, target) => {
        const sIndex = list.findIndex(i => i.id === source);
        const tIndex = list.findIndex(i => i.id === target);

        list.splice(tIndex, 0, list.splice(sIndex, 1)[0]);

        onSort(list);
    }

    const styleCss = {
        width: '250px'
    }

    return (
        <div ref={wrapRef} style={styleCss} className={`drag ${onDrag ? 'on-drag' : ''}`}>
            <Box>
                <Margin>
                    <Between>
                        <h4>{title}</h4>
                        <LinkBtn i="add" onClick={onAdd}>Add New</LinkBtn>
                    </Between>
                </Margin>
                <RowsShort>
                    {list.map((i, index) => (
                        <div key={i.id} className="item" data-id={i.id}>
                            <Between>
                                <LinkBtn
                                    selected={index === selected}
                                    onClick={() => onSelect(index)}
                                >
                                    {i.title}
                                </LinkBtn>
                                {list.length > 1 && <LinkBtn i="delete" onClick={() => onDelete(index)}></LinkBtn>}
                            </Between>
                        </div>
                    ))}
                </RowsShort>
            </Box>
        </div>
    )
}