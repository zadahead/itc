import { useSelector } from "react-redux";
import { Between, Rows, Container } from "UIKit";

export const Subjects = () => {
    const [jobs, requirements, responsibilities] = useSelector(state => [state.jobs, state.requirements, state.responsibilities]);

    const map = {}
    const setMap = it => {
        if (!map[it]) { map[it] = 0; }
        map[it]++;
    }
    jobs.forEach(job => {
        job?.requirements?.forEach(setMap);
        job?.responsibilities?.forEach(setMap);
    });

    const list = [...requirements, ...responsibilities];

    list.forEach(job => {
        job.sum = map[job.id] || 0;
    });

    list.sort((a, b) => {
        return a.sum < b.sum ? 1 : -1;
    })

    return (
        <Rows>
            <Container title="Subjects">
                {list && list.map(job => (
                    <Between key={job.id}>
                        <p>{job.title}</p>
                        <p>{job.sum}</p>
                    </Between>
                ))}
            </Container>
        </Rows>
    )
}