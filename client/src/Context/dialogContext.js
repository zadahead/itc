import { createContext, useEffect, useState } from "react";
import { createPortal } from "react-dom";
import { Dialog } from "UIKit";

export const dialogContext = createContext({});

const Provider = dialogContext.Provider;

export const DialogProvider = ({ children }) => {
    const [data, setData] = useState(null);

    useEffect(() => {
        window.conf = (m) => set(m, 'confirm');
        window.showPic = (src) => set(src, 'pic');
    }, [])

    useEffect(() => {
        if (data) {
            window.addEventListener('keyup', handleKeys);
        } else {
            window.removeEventListener('keyup', handleKeys);
        }
    }, [data])

    const set = (message, type) => {
        return new Promise((res) => {
            const req = getByType(type, res);
            req.message = message;
            req.type = type;
            setData(req);
        })
    }

    const getByType = (type, res) => {
        switch (type) {
            case "confirm": return {
                onOk: () => {
                    res(true);
                    close();
                },
                onCancel: () => {
                    res(false);
                    close();
                }
            }
            case "pic": return {
                onClose: () => {
                    res(false);
                    close();
                }
            }
        }
    }

    const handleKeys = (e) => {
        switch (e.keyCode) {
            case 13: { //Enter
                return triggerAction(['onOk']);
            }
            case 27: { //Esc
                return triggerAction(['onCancel', 'onClose', 'onOk']);
            }
            default:
                break;
        }
        console.log(e.keyCode);
    }

    const triggerAction = (actions) => {
        for (let i = 0; i < actions.length; i++) {
            const action = actions[i];
            if (data[action]) {
                return data[action]();
            }
        }
    }

    const close = () => {
        setData(null);
    }

    const value = {
        data,
        setData
    }

    return (
        <Provider value={value}>
            {data && <ShowDialog {...data} />}
            {children}
        </Provider>
    )
}

const ShowDialog = (props) => {
    return (
        createPortal((
            <Dialog {...props} />
        ), document.getElementById('modal'))
    )
}