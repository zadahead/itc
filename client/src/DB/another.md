## About

#### <Tree /

```js

export const Tree = (props) => {
    const [onDrag, setOnDrag] = useState('');

    const timerRef = useRef();
    const sourceRef = useRef();
    const wrapRef = useRef();

    useEffect(() => {

        wrapRef.current.addEventListener('mousedown', handleDown);
        wrapRef.current.addEventListener('mouseup', handleUp);

        return () => {
            wrapRef.current.removeEventListener('mousedown', handleDown);
            wrapRef.current.removeEventListener('mouseup', handleUp);
        }

    }, [props.list])

    const handleDown = (e) => {
        const item = e.target.closest('.item');
        if (item) {
            timerRef.current = setTimeout(() => {
                sourceRef.current = item.getAttribute('data-id');
                setOnDrag(true);
            }, 300);
        }
    }

    const handleUp = (e) => {
        clearTimeout(timerRef.current);

        if (sourceRef.current) {
            const item = e.target.closest('.item');
            if (item) {
                const source = JSON.parse(sourceRef.current);
                const target = JSON.parse(item.getAttribute('data-id'));
                handleChange(source, target);
            }
            setOnDrag(false);
            sourceRef.current = null;
        }
    }

    const handleChange = (source, target) => {
        let nodes = [...props.list];
        try {
            const [sNode, sParent, sIndex] = getNode(source, nodes);
            const n = { ...sNode }
            sParent.splice(sIndex, 1);

            const [tNode, tParent, tIndex] = getNode(target, nodes);
            console.log(tParent);
            tParent.splice(tIndex + 1, 0, n);

            props.onChange(nodes);
        } catch (error) {

        }
    }

    const handleToggle = (id) => {
        let nodes = [...props.list];
        const [node] = getNode(id, nodes);

        node.open = !node.open;
        props.onChange(nodes);
    }

    const getNode = (id, nodes) => {
        const items = nodes?.items || nodes;
        if (items?.length) {
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                if (item.id === id) {
                    return [item, items, i]
                }
                const inner = getNode(id, item.items);
                if (inner) {
                    return inner;
                }
            }
        }
        return false;
    }

    return (
        <div className={`Tree ${onDrag ? 'on-drag' : ''}`} ref={wrapRef}>
            <TreeChild {...props} onChange={handleToggle} />
        </div>
    )
}
```

#### <TreeChild /

```js
const TreeChild = ({ list, path = [], onChange }) => {



    const handleToggle = (id) => {
        onChange(id);
    }

    const renderList = () => {
        return list.map((i, index) => {
            return (
                <div key={i.id}>
                    <div
                        className="item"
                        style={{
                            paddingLeft: `calc(var(--gap) * ${path.length + 1})`
                        }}
                        onClick={() => handleToggle(i.id)}
                        data-id={i.id}
                    >
                        <Line>
                            <Icon
                                i={`${i.open ? 'angle-down' : 'angle-right'}`}
                                visibility={i.items?.length ? true : false}
                            />
                            <Icon i="cog" />
                            <h4>{i.title}</h4>
                        </Line>
                    </div>
                    {i.items && i.open && <TreeChild list={i.items} path={[...path, index]} onChange={onChange} />}
                </div>
            )
        })
    }

    return renderList()
}

```

#### Tree.css

```css
.Tree .item {
    padding: var(--gap-m);
    border-bottom: 2px solid transparent;
}

.Tree.on-drag .item:hover {
    border-bottom: 2px solid #e1e1e1;
    cursor: grab;
}

.Tree:not(.on-drag) .item:hover {
    background-color: #f1f1f1;
    cursor: pointer;
}

.Tree:not(.on-drag) .item:hover h4 {
    color: var(--color_primary);
}

.Tree * {
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -o-user-select: none;
    user-select: none;
}

.Tree ::selection {
    background: transparent;
}

.Tree ::-moz-selection {
    background: transparent;
}

```

#### use case 

```js
const tree = [
    {
        id: 1,
        title: "mosh1",
        open: true,
        items: [
            {
                id: 2, title: 'gever', items: [
                    { id: 3, title: "mosh" }
                ]
            },
            { id: 4, title: 'gever2' }
        ]
    },
    { id: 5, title: "mosh2" },
    {
        id: 6,
        title: "david3",
        items: [
            { id: 7, title: 'gever2' }
        ]
    }
]
```

```js
const [treeList, setTreeList] = useState(tree);

return (
        <Tree
            list={treeList}
            onChange={setTreeList}
          />
)
```