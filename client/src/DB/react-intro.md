# Hello React

_A step-by-step React course - with all the things you need to get started with the most advanced and popular web framework tool out there_

## Prerequisites

1. Install VSCode (or other IDE)
2. Install NodeJs (and npm)
3. Make sure you can use your (IDE or cmd) terminal to write `npm --version` and `node --version`

![](https://icatcare.org/app/uploads/2018/07/Thinking-of-getting-a-cat.png)
> this cat is awsome 


## Points

1. Open Your Camera
2. Ask Questions 
3. Do it yourself


## Course Agenda

1. Create React App
1. What is JSX ?
1. Functional Components
1. Props
1. Export / Import Component
1. Props Children
1. Layouts
    - Grid 
    - Line 
1. Elements
    - Icon
1. Handling Events
1. Elements
    - Btn
    - Btn + Icon
1. Prepare UIKit
1. React Routing
1. States
1. Two way bindings (Input)
1. Components Reuse
1. Conditional Rendering
1. Lists and Keys
1. React Life Cycle
1. API Calls – axios
1. React useRef
1. DropDown List
1. React Custom Hooks
1. Context
1. React Redux
    - thunk 
1. Todo List
    - READ 
    - UPDATE 
    - CREATE 
    - DELETE 


### I see you (if we get there...) - Bonus Tracks
1. Additional Hooks
1. user Auth
1. Class Components
1. React Portals - Modal Dialogs
1. Form Validators 
1. Files Tree
1. Undo Redo
1. React Testing Library
1. Cypress
1. Redux Toolkit
1. Build and Deploy Project
1. css `@media`
1. css animations