export const insertAtCursor = (input, val) => {
    //IE support
    if (document.selection) {
        input.focus();
        const sel = document.selection.createRange();
        sel.text = val;
    }
    //MOZILLA and others
    else if (input.selectionStart || input.selectionStart === '0') {
        const startPos = input.selectionStart;
        const endPos = input.selectionEnd;
        input.value = input.value.substring(0, startPos)
            + val
            + input.value.substring(endPos, input.value.length);
    } else {
        input.value += val;
    }
}

export const removeAtCursor = (input, val) => {
    //IE support
    if (document.selection) {
        input.focus();
        const sel = document.selection.createRange();
        sel.text = val;
    }
    //MOZILLA and others
    else if (input.selectionStart || input.selectionStart === '0') {
        const startPos = input.selectionStart - val.length;
        const endPos = input.selectionEnd;
        input.value = input.value.substring(0, startPos)
            + input.value.substring(endPos, input.value.length);
    } else {
        input.value += val;
    }
}

export const getDomainFromUrl = (url) => {
    var a = document.createElement('a');
    a.setAttribute('href', url);
    return a.hostname;
}