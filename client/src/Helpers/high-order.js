export const getNextSort = (list) => {
    return list.reduce((state, { sort }) => {
        return Math.max(state, sort + 1)
    }, 0);
}