import { useState } from "react"

export const useInput = (initialState, placeholder) => {
    const [value, setValue] = useState(initialState || '');

    const empty = () => setValue('');

    return {
        value,
        onChange: setValue,
        p: placeholder,
        empty
    }
}