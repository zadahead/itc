import { useDispatch, useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getState } from '../State/helpers';
import { saveJobs } from '../State/jobs';
import { Btn, Logo, Between, Line } from 'UIKit';
import { useState } from 'react';
import { useEffect } from 'react';
import axios from 'axios';
import { loadTabs } from 'State/docs';

export const Header = () => {
    const dispatch = useDispatch();
    const { tabs } = useSelector(state => state.docs);

    useEffect(() => {
        dispatch(loadTabs());
    }, [])


    const handleSave = () => {
        dispatch(saveJobs(getState()));
    }

    return (
        <Between>
            <Line>
                <Logo />
                <NavLink to='/home'>Home</NavLink>
                {tabs.map(i => (
                    <NavLink key={i.id} to={`/docs/${i.url}`}>{i.title}</NavLink>
                ))}
                {/* <NavLink to='/seek'>Job Seek</NavLink> */}
            </Line>
            <Line>
            </Line>
        </Between>
    )
}