import axios from 'axios';

const baseUrl = process.env.REACT_APP_API;

export const GET = (type, path, body) => fetchUrl(type, path, 'get', body);
export const POST = (type, path, body) => fetchUrl(type, path, 'post', body);

export const fetchUrl = (type, path, method, body) => {

    return async dispatch => {
        try {
            var data = await fetchAxios(method, path, body);

            dispatch({
                type,
                payload: data
            })

        } catch (error) {
            console.log(error);
        }
    }
}

export const axiosGet = (path, body) => fetchAxios('get', path, body);
export const axiosPost = (path, body) => fetchAxios('post', path, body);
export const axiosPatch = (path, body) => fetchAxios('patch', path, body);
export const axiosDelete = (path, body) => fetchAxios('delete', path, body);

export const fetchAxios = async (method, path, body) => {
    const resp = await axios[method](`${baseUrl}${path}`, body);
    return resp.data;
}