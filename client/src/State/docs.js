import { axiosGet } from "./axios";

const { ZedKit } = require("./zedkit");

const docs = new ZedKit(
    {
        list: [],
        selected: null,
        tabs: [],
        subjects: {}
    },
    {
        setList: (state, payload) => {
            state.list.push(payload);
        },
        loadTabs: [
            async () => await axiosGet('/docs'),
            (state, payload) => {
                state.tabs = payload;
            }
        ],
        loadSubjects: [
            async (path) => {
                return await axiosGet(`${path}`);
            },
            (state, payload) => {
                state.subjects = payload;
            }
        ],
        setSubjects: (state, payload) => {
            state.subjects.items = payload
        },
        loadThis: [
            async () => {
                const data = await axiosGet('/jobseek');

                return data;
            },
            (state, payload) => {
                console.log('state', state);
                console.log('payload', payload);
                state.list.push(payload);
            }
        ],
        setSelected: (state, item) => {
            state.selected = item;
        }
    }
)

export const { setList, loadThis, setSelected, loadTabs, loadSubjects, setSubjects } = docs.actions();
export const docsReducer = docs.reducer;