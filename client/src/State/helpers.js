
export const act = (type) => {
    return (payload) => ({ type, payload });
}

export const setState = (state) => {
    localStorage.setItem('ITC-DATA', JSON.stringify(state));
}

export const getState = () => {
    const item = localStorage.getItem('ITC-DATA');
    if (item) {
        return JSON.parse(item);
    }
    return {}
}
