import { GET, POST } from "./axios";
import { act, getState } from "./helpers";

export const addJob = act('ADD_JOB');
export const selectJob = act('GET_JOB');
export const newJob = act('NEW_JOB');
export const setJob = act('SET_JOB');
export const getJobs = GET('GET_JOBS', '/jobseek');
export const saveJobs = (body) => POST('SAVE_JOBS', '/jobseek/save', body);

export const jobsReducer = (jobs = [], action) => {
    switch (action.type) {
        case 'ADD_JOB':
            return [...jobs, action.payload];
        case 'GET_JOBS':
            return action.payload?.jobs;
        case 'SET_JOB': {
            return jobs.map(j => {
                if (j.id === action.payload.id) {
                    return action.payload;
                }
                return j;
            });
        }
        default:
            return jobs;
    }
}

export const jobReducer = (job = {}, action) => {
    switch (action.type) {
        case 'GET_JOB': {
            const { jobs } = getState();
            const id = action.payload;
            return jobs.find(j => j.id === id);
        }
        case 'NEW_JOB': {
            return {};
        }
        default:
            return job;
    }
}