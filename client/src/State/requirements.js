import { act, getState } from "./helpers";

export const addRequirements = act('ADD_REQUIREMENT');

export const requirementsReducer = (list = [], action) => {
    switch (action.type) {
        case 'ADD_REQUIREMENT':
            return [...list, action.payload];
        case 'GET_JOBS':
            return action.payload?.requirements;
        default:
            return list;
    }
}