import { act } from "./helpers";

export const addResponsibilities = act('ADD_RESPONS');

export const responsibilitiesReducer = (list = [], action) => {
    switch (action.type) {
        case 'ADD_RESPONS':
            return [...list, action.payload];
        case 'GET_JOBS':
            return action.payload?.responsibilities;
        default:
            return list;
    }
}