
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from "redux";
import { setState } from './helpers';
import { jobReducer, jobsReducer } from "./jobs";
import { requirementsReducer } from './requirements';
import { responsibilitiesReducer } from './responsibilities';

import ReduxThunk from "redux-thunk";
import { docsReducer } from './docs';

const reducers = combineReducers({
    jobs: jobsReducer,
    job: jobReducer,
    requirements: requirementsReducer,
    responsibilities: responsibilitiesReducer,
    docs: docsReducer
})

const middleware = ({ getState }) => {
    return (next) => {
        return (action) => {
            next(action);
            setState(getState());
        }
    }
}

const store = createStore(reducers, applyMiddleware(middleware, ReduxThunk))

const Store = ({ children }) => {
    return (
        <Provider store={store}>
            {children}
        </Provider>
    )
}

export default Store;

