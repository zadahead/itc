import { act } from "./helpers";

export class ZedKit {
    constructor(initialState, struct) {
        this.initialState = initialState;
        this.struct = struct;
        this.funcs = {};

        this.set();
    }

    isPromise = (p) => {
        return p.constructor.name === 'AsyncFunction';
    }

    set = () => {
        for (const x in this.struct) {
            const f = this.struct[x];
            if (Array.isArray(f)) {
                const [start, end] = f;
                this.funcs[x] = start;
                this.funcs[x + 'End'] = end;
            } else {
                this.funcs[x] = f;
            }
        }
        return this.funcs;
    }

    actions = () => {
        let map = {};
        for (const x in this.struct) {
            const f = this.struct[x];
            if (Array.isArray(f)) {
                const [start] = f;

                map[x] = (payload) => {
                    return async dispatch => {
                        const resp = await start(payload);
                        dispatch({
                            type: x + 'End',
                            payload: resp
                        })

                    }
                };
            } else {
                map[x] = act(x);
            }
        }
        return map;
    }

    reducer = (state = this.initialState, action) => {
        const func = this.funcs[action.type];

        if (func) {
            func(state, action.payload);
            return { ...state };
        }
        return state;
    }
}