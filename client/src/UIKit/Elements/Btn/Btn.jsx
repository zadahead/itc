import { Between } from "UIKit";
import { Icon } from "../Icon/Icon";
import "./Btn.css";

export const Btn = ({ children, onClick, type, selected, i, disabled, theme }) => {
    return (
        <button
            className="Btn"
            onClick={onClick}
            data-type={type}
            data-selected={selected}
            data-theme={theme}
            disabled={disabled}
            data-testid="btn"
        >
            <Between>
                {i && <Icon i={i} />}
                <div>
                    {children}
                </div>
            </Between>
        </button>
    )
}

export const CheckBtn = (props) => {
    return <Btn {...props} type="check" />
}

export const LinkBtn = (props) => {
    return <Btn {...props} type="link" />
}

export const IconBtn = (props) => {
    return (
        <Btn {...props} i={null} type="icon">
            <Icon i={props.i} />
        </Btn>
    )
}

export const ItemSelect = (props) => {
    return <Btn {...props} type="item-select" />
}