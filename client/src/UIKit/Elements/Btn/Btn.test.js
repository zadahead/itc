import { render, screen } from "@testing-library/react";
import "@testing-library/jest-dom";
import { Btn } from "./Btn";

describe('<Btn />', () => {
    it('will render Btn', () => {
        render(<Btn />);

        const btn = screen.getByTestId('btn');

        expect(btn).toBeInTheDocument();
    })
})