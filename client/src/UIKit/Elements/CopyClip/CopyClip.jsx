import { useState } from "react";
import CopyToClipboard from "react-copy-to-clipboard"
import { IconBtn } from "UIKit"

import './CopyClip.css';

export const CopyClip = ({ children, text }) => {
    const [isCopied, setIsCopied] = useState(false);

    const handleCopy = () => {
        setIsCopied(true);
        setTimeout(() => {
            setIsCopied(false);
        }, 500);
    }

    return (
        <div className="CopyClip">
            <div>
                {children}
            </div>
            <div className="action">
                <CopyToClipboard text={text}
                    onCopy={handleCopy}>
                    <IconBtn i={isCopied ? 'done' : 'content_copy'} selected={isCopied} />
                </CopyToClipboard>
            </div>
        </div>
    )
}