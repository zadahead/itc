import { useInput } from 'Hooks/useInput';
import { Link } from 'react-router-dom';
import { Absolute, Center, Icon, Input, NoWrap } from 'UIKit';
import './Cube.css';

export const Cube = ({ children }) => {
    return (
        <div className='Cube'>
            {children}
        </div>
    )
}

export const CubeIcon = ({ src, text, to, actions, isEdit, onChange }) => {
    const Text = useInput(text, 'doc title...');
    return (
        <Absolute
            elem={actions}
            stretch
        >
            {isEdit ? (
                <Cube>
                    <Center>
                        <NoWrap>
                            <Input {...Text} />
                            <NoWrap>
                                <Icon i="done" unBubble onClick={() => onChange(Text.value)} />
                                <Icon i="close" unBubble onClick={() => onChange(text)} />
                            </NoWrap>
                        </NoWrap>
                    </Center>
                </Cube>
            ) : (
                <Link to={to}>
                    <Cube>
                        <div className='CubeIcon'>
                            {src && <img src={src} alt={src} />}
                            <h4>{text}</h4>
                        </div>
                    </Cube>
                </Link>
            )}
        </Absolute>

    )
}