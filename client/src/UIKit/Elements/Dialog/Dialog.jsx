import { Btn, Center, CenterAlign, Line, MarginH, Padding, Rows, Short } from 'UIKit';
import './Dialog.css';

export const Dialog = ({ type, message, onOk, onCancel, onClose }) => {

    const renderContent = () => {
        switch (type) {
            case "pic": return (
                <img src={message} alt={message} />
            )
            default: return <h3>{message}</h3>
        }
    }
    return (
        <div className="Dialog" data-type={type}>
            <div className="content">
                <Rows>
                    <MarginH>
                        <CenterAlign>
                            {renderContent()}
                        </CenterAlign>
                    </MarginH>
                    <Padding>
                        <Center>
                            <Short>
                                {onCancel && <Btn theme="cancel" onClick={onCancel}>Cancel</Btn>}
                                {onClose && <Btn theme="cancel" onClick={onClose}>Close</Btn>}
                                {onOk && <Btn onClick={onOk}>OK</Btn>}
                            </Short>
                        </Center>
                    </Padding>
                </Rows>
            </div>
        </div>
    )
}