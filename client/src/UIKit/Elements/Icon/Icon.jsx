import './Icon.css';
import logo from 'Images/logo.png';

export const Icon = ({ i, visibility, onClick, unBubble }) => {
    const handleClick = (e) => {
        if (unBubble) {
            e.stopPropagation();
        }

        if (onClick) {
            onClick(e);
        }
    }
    return (
        <span
            className={`Icon material-symbols-outlined ${onClick ? 'action' : ''}`}
            data-visibility={visibility}
            onClick={handleClick}
        >
            {i}
        </span>
    )
}

export const Logo = () => {
    return <img className='Logo' src={logo} alt="ITC" />
}