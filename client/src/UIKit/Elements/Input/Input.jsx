import { forwardRef } from 'react';
import './Input.css';

export const Input = forwardRef((props, ref) => {
    const { empty, p, onChange, ...rest } = props;

    const handleChange = (e) => {
        console.log('bbbb')
        onChange(e.target.value);
    }

    return (
        <div className='Input'>
            <input ref={ref} {...rest} placeholder={p} onChange={handleChange} />
        </div>
    )
})