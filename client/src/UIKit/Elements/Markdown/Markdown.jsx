import { CopyClip } from "UIKit";
import ReactMarkdown from 'react-markdown';
import remarkGfm from 'remark-gfm';
import codesandbox from 'remark-codesandbox';

import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark } from 'react-syntax-highlighter/dist/esm/styles/prism';

import "./Markdown.css";
import { getDomainFromUrl } from "Helpers/dom";
export const Markdown = ({ markdown }) => {

    const handleShowPic = (src) => {
        window.showPic(src);
    }

    return (
        <div className="Markdown">
            <ReactMarkdown children={markdown}
                remarkPlugins={[remarkGfm]}
                components={{
                    a({ node, inline, className, children, ...props }) {
                        const { href } = node.properties;

                        const youtubeSrc = href.match(/youtube.com\/watch\?v=([A-Za-z0-9\_]*)/);

                        if (youtubeSrc) {
                            return (
                                <iframe src={`https://www.youtube.com/embed/${youtubeSrc[1]}`} title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            )
                        }

                        return <a target="_blank" href={href}>{getDomainFromUrl(href)}</a>;
                    },
                    img({ node, inline, className, children, ...props }) {
                        const src = node.properties.src;


                        const root = !src.startsWith('http') ? process.env.REACT_APP_Z3 + '/' : '';

                        return (
                            <img src={root + src} alt={src} onClick={() => handleShowPic(root + src)} />
                        )

                    },
                    code({ node, inline, className, children, ...props }) {
                        const match = /language-(\w+)/.exec(className || '')
                        return !inline && match ? (
                            <CopyClip text={children}>
                                <SyntaxHighlighter
                                    children={String(children).replace(/\n$/, '')}

                                    language={match[1]}
                                    PreTag="div"
                                    style={atomDark}
                                    {...props}
                                    showLineNumbers={true}
                                    wrapLines={true}
                                />
                            </CopyClip>
                        ) : (
                            <code className={className} {...props}>
                                {children}
                            </code>
                        )
                    }
                }}
            />
        </div>
    )
}