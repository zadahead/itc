import { useInput } from 'Hooks/useInput';
import { useState } from 'react';
import { useRef } from 'react';
import { useEffect } from 'react';
import { createPortal } from 'react-dom';
import { axiosGet, axiosPost } from 'State/axios';
import { Between, Icon, Input, Line, Rows, RowsShort, Short, Spread, SpreadEnd } from 'UIKit';
import './SearchBlock.css';

export const SearchBlock = ({ target, onClose, onSelect, isExist }) => {
    const [pos, setPos] = useState({});
    const [list, setList] = useState([]);
    const timerRef = useRef();
    const inputRef = useRef();

    const Search = useInput('', 'Search doc...');

    useEffect(() => {
        const { top, right } = target.current.getBoundingClientRect();

        setPos({
            left: right + 'px',
            top: top + 'px'
        })

        inputRef.current.focus();
    }, [])

    useEffect(() => {
        clearTimeout(timerRef.current);

        timerRef.current = setTimeout(async () => {

            if (Search.value) {
                const resp = await axiosPost('/doc/search', { val: Search.value});

                setList(resp)
            } else {
                setList([])
            }
        }, 300)
    }, [Search.value])

    const handleClick = (e) => {
        e.stopPropagation();
    }

    const getBold = (val) => {
        const rr = val.replace(new RegExp(`(${Search.value})`, 'ig'), `<b>$1</b>`);
        return rr;
    }

    const renderTitle = (item) => {
        return  (
            <RowsShort>
                <h4 className='title' dangerouslySetInnerHTML={{ __html: getBold(item.title)}} />
                <Short>
                    {item.docs.map((i)=> (
                        <h6 key={i.id} className='docs' dangerouslySetInnerHTML={{ __html: getBold(i.title)}} />
                    ))}
                </Short>
                <Short>
                    {item?.tags?.map((i)=> (
                        <h6 key={i} className='docs' dangerouslySetInnerHTML={{ __html: getBold(i)}} />
                    ))}
                </Short>
            </RowsShort>
        )
    }

    const handleSelect = (i) => {
        onSelect({
            data: i.id,
            title: i.title
        })
    }

    return (
        createPortal((
            <div className='SearchBlock' onClick={handleClick}>
                <div className='inner' style={pos}>
                    <div className='header'>
                        <Between>
                            <SpreadEnd>
                                <Icon i="search" />
                                <Input {...Search} ref={inputRef}/>
                            </SpreadEnd>
                            <Icon i="close" onClick={onClose} />
                        </Between>
                    </div>
                    <ul className='list'>
                        {list.map((i) => (
                            <li key={i.id} className={`${isExist(i.id) ? 'exist' : ''}`} onClick={() => handleSelect(i)}>{renderTitle(i)}</li>
                        ))}
                    </ul>
                </div>
            </div>
        ), document.getElementById('screen'))
    )
}