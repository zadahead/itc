import { Short } from 'UIKit';
import { Icon } from '../Icon/Icon';
import './Tabs.css';

export const Tabs = ({ list, selected = 0, onChange }) => {
    const getContent = () => {
        const item = list[selected];

        return item ? item.content : list[0].content;
    }

    return (
        <div className='Tabs'>
            <div className='header'>
                {list.map((i, index) => (
                    <Tab
                        id={index}
                        key={index}
                        text={i.text}
                        icon={i.icon}
                        selected={index === selected}
                        onChange={onChange}
                    />
                ))}
            </div>
            <div className='content'>
                {getContent()}
            </div>
        </div>
    )
}

export const Tab = ({ id, text, icon, selected, onChange }) => {
    const handleChange = () => {
        onChange(id);
    }
    return (
        <div className='Tab' data-selected={selected} onClick={handleChange}>
            <Short>
                <Icon i={icon} />
                <div>
                    {text}
                </div>
            </Short>
        </div>
    )
}