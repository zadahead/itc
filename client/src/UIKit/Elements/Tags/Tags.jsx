import { useInput } from 'Hooks/useInput';
import { Icon, Input, Line, Short, SpreadEnd } from 'UIKit';
import './Tags.css';

export const Tags = ({ list = [], onChange }) => {
    const InputField = useInput('', 'Add Tag..');

    const set = new Set();
    list.forEach(item => {
        set.add(item);
    });

    const handleRemove = (item) => {
        set.delete(item);
        onChange(Array.from(set));
    }

    const handleAdd = (item) => {
        set.add(item);
        onChange(Array.from(set));
    }

    const handleEnter = (e) => {
        if(e.keyCode === 13) {
            handleAdd(InputField.value);
            InputField.onChange('');
        }
    }

    return (
        <div className='Tags'>
            <SpreadEnd>
                {Array.from(set).map(i => (
                    <Tag key={i} text={i} onClose={() => handleRemove(i)}  />
                ))}
                <Input {...InputField} onKeyUp={handleEnter} />
            </SpreadEnd>
        </div>
    )
}

export const Tag = ({ text, onClose }) => {
    return (
        <div className='Tag' onClick={onClose} >
            <Short>
                <h5>{text}</h5>
                <Icon i="close" />
            </Short>
        </div>
    )
}