import { forwardRef } from 'react';
import './TextBox.css';

export const TextBox = forwardRef(({ onChange, value }, ref) => {
    const handleText = (e) => {
        onChange(e.target.value);
    }

    return (
        <div className='TextBox'>
            <textarea ref={ref} onChange={handleText} value={value} />
        </div>
    )
})