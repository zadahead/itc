import { getNextSort } from "Helpers/high-order";
import { useEffect, useRef, useState } from "react";
import { Between, Btn, Icon, Input, Line, Rows, SearchBlock, Short } from "UIKit";

import './Tree.css';

export const Tree = (props) => {
    const [onDrag, setOnDrag] = useState('');
    const [needSave, setNeedSave] = useState(false);

    const timerRef = useRef();
    const sourceRef = useRef();
    const wrapRef = useRef();

    const { items: list } = props.parent;



    useEffect(() => {
        if (!wrapRef.current) { return }

        wrapRef.current.addEventListener('mousedown', handleDown);
        wrapRef.current.addEventListener('mousemove', handleHover);
        wrapRef.current.addEventListener('mouseup', handleUp);

        return () => {
            if (wrapRef?.current) {
                wrapRef.current.removeEventListener('mousedown', handleDown);
                wrapRef.current.removeEventListener('mousemove', handleHover);
                wrapRef.current.removeEventListener('mouseup', handleUp);
            }
        }

    }, [list])

    useEffect(() => {
        if (!list) { return }

        const track = {}
        openAll(props.selected, list, track);

        if (track.changed) {
            handleSaveDraft();
        }

    }, [props.selected, list])



    const handleDown = (e) => {
        const item = e.target.closest('.item');
        if (item) {
            timerRef.current = setTimeout(() => {
                sourceRef.current = item.getAttribute('data-id');
                setOnDrag(true);
            }, 300);
        }
    }

    const handleUp = (e) => {
        clearTimeout(timerRef.current);

        if (sourceRef.current) {
            const item = e.target.closest('.item');
            if (item) {
                const source = sourceRef.current;
                const target = item.getAttribute('data-id');
                const isCenter = item.classList.contains('center');

                handleChange(source, target, isCenter);
            }
            setOnDrag(false);
            sourceRef.current = null;
        }
    }

    const handleHover = (e) => {
        if (sourceRef.current) {
            const item = e.target.closest('.item');
            if (item) {

                const { top, height } = item.getBoundingClientRect();
                const per = ((e.y - top) / height) * 100;

                if (per < 70) {
                    item.classList.add('center');
                } else {
                    item.classList.remove('center');
                }
            }
        }
    }

    const handleChange = (source, target, isCenter) => {
        if (source === target) { return; }

        let nodes = { ...props.parent };
        try {
            const [sNode, sItems, sIndex, sParent] = getNode(source, nodes);
            const n = { ...sNode }
            sItems.splice(sIndex, 1);

            const [, tItems, tIndex, tParent] = getNode(target, nodes, null, isCenter);
            tItems.splice(tIndex + 1, 0, n);

            props.onSort(sParent, tParent);

            handleSaveDraft(false, nodes.items);
            props.onselect(n);
        } catch (error) {
            console.error(error);
        }
    }

    const handleToggle = (item) => {
        if (!item.data) {
            item.open = !item.open;
            handleSaveDraft(true);
        } else {
            props.onselect(item);
        }
    }

    const handleAdd = (item, bind) => {
        if (!item.items) {
            item.items = [];
        }

        const newItem = getNewItem('New Doc');
        newItem.data = 'new';
        newItem.sort = getNextSort(item.items);

        if (bind) {
            newItem.data = bind.data;
            newItem.title = bind.title;
            bind.sort = newItem.sort;
        }


        item.items.push(newItem)
        handleSaveDraft();
        props.onselect(newItem);
        props.onAdd(newItem, item.id, bind);
    }

    const handleAddTop = () => {
        const newItem = getNewItem('New Folder');
        newItem.items = [];

        newItem.sort = getNextSort(list);

        list.push(newItem);

        handleSaveDraft();
        props.onselect(newItem);
        props.onAdd(newItem);
    }

    const getNewItem = (title) => (
        {
            id: new Date().toJSON(),
            isnew: true,
            title: title,
            items: []
        }
    )
    const handleRemove = async (item) => {
        const approve = await window.conf("This will remove item completely, continue?");

        if (approve) {
            const [, sParent, sIndex] = getNode(item.id, props.parent);
            props.onItemDelete(item);

            sParent.splice(sIndex, 1);

            handleSaveDraft();
            if (sParent.length) {
                const next = sParent.find(i => i.data);
                props.onselect(next);
            } else {
                props.onselect(null);
            }
        }
    }

    const handleSet = (item, value) => {
        item.title = value;
        delete item.isnew;

        props.onItemChange(item);
        handleSaveDraft();
    }

    const handleSaveDraft = (isNoNeedSave, nodes) => {
        props.onChange(nodes || [...list]);
        if (!isNoNeedSave) {
            setNeedSave(true);
        }
    }

    const handleCancel = () => {
        setNeedSave(false);
        props.onCancel();
    }


    const openAll = (id, node, track) => {
        for (let i = 0; i < node.length; i++) {
            const item = node[i];

            if (item.id === id) {
                return item;
            }
            if (item.items) {
                const inner = openAll(id, item.items, track);
                if (inner) {
                    if (!item.open) {
                        item.open = true;
                        track.changed = true;
                    }
                    return inner;
                }
            }
        }
    }

    const getNode = (id, nodes, parent, isAppend) => {
        const items = nodes?.items || nodes;
        if (items?.length) {
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                if (item.id === id) {
                    const p = parent || nodes;
                    if (!item.data) {
                        if (isAppend) {
                            return [item, item.items, i, item];
                        }
                        return [item, p.items, i, p];
                    }
                    return [item, items, i, parent || nodes];
                }
                const inner = getNode(id, item.items, item, isAppend);
                if (inner) {
                    return inner;
                }
            }
        }
        return false;
    }

    const getNodeByData = (id, nodes) => {
        const items = nodes?.items || nodes;
        if (items?.length) {
            for (let i = 0; i < items.length; i++) {
                const item = items[i];
                if (item.data === id) {
                    return item;
                }
                const inner = getNodeByData(id, item.items);
                if (inner) {
                    return inner;
                }
            }
        }
        return false;
    }

    const isExist = (id) => {
        const it = getNodeByData(id, props.parent);
        return it;
    }

    if (!list) { return null }

    return (
        <div className={`Tree ${onDrag ? 'on-drag' : ''}`} ref={wrapRef}>
            <Rows>
                <Between>
                    <Short>
                        <h5>{props.parent.title}</h5>
                    </Short>
                    <Short>
                        <Icon i="add" onClick={handleAddTop} />
                    </Short>
                </Between>
                <div className="container">
                    <TreeChild
                        {...props}
                        handleToggle={handleToggle}
                        handleAdd={handleAdd}
                        handleRemove={handleRemove}
                        handleSet={handleSet}
                        isExist={isExist}
                    />
                </div>
            </Rows>
        </div>
    )
}

const TreeChild = (props) => {
    const { parent: { items: list }, path = [] } = props;

    const renderList = () => {
        return list.map((i, index) => {
            return (
                <div key={i.id}>
                    <Child {...props} i={i} />
                    {i.items.length > 0 && i.open && (
                        <TreeChild
                            {...props}
                            parent={i}
                            path={[...path, index]}
                        />
                    )}
                </div>
            )
        })
    }

    return renderList()
}

const Child = ({
    selected,
    i,
    handleToggle,
    handleAdd,
    path = [],
    handleRemove,
    handleSet,
    isExist
}) => {
    const [isHover, setIshover] = useState(false);
    const [isBind, setIsBind] = useState(false);
    const [isEdit, setIsEdit] = useState(i.isnew);
    const [value, setValue] = useState('');
    const inputRef = useRef();
    const childRef = useRef();

    useEffect(() => {
        setValue(i.title);
    }, [i.title])

    useEffect(() => {
        if (isEdit) {
            setTimeout(() => {
                inputRef.current.select();
            }, 300);
        }
    }, [isEdit])

    const handleChange = (t) => {
        setValue(t);
    }

    const handleSave = () => {
        setIsEdit(false);
        handleSet(i, value);
    }

    const handleEnter = (e) => {
        if(e.keyCode === 13) {
            handleSave();
        }
    }

    const handleBind = () => {
        setIsBind(true);
    }

    const handleBindClose = () => {
        setIsBind(false);
    }

    const handleBindAdd = (i, bind) => {
        handleAdd(i, bind);
    }

    return (
        <div
            ref={childRef}
            className={`item ${selected === i.id ? 'selected' : ''} ${!i.data ? 'folder' : ''}`}
            style={{
                paddingLeft: `calc(var(--gap) * ${path.length + 1})`
            }}
            onClick={isEdit ? null : () => handleToggle(i)}
            data-id={i.id}
            onMouseEnter={() => setIshover(true)}
            onMouseLeave={() => setIshover(false)}
        >
            {isEdit ? (
                <Between>
                    <Input ref={inputRef} value={value} onChange={handleChange} onKeyUp={handleEnter} />
                    <Icon unBubble i="done" onClick={handleSave} />
                </Between>
            ) : (
                <Short>
                    <Icon
                        i={`${i.open ? 'expand_more' : 'chevron_right'}`}
                        visibility={!i.data ? true : false}
                    />
                    {!i.data ? <h4>{i.title}</h4> : <h5>{i.title}</h5>}
                    {isHover && (
                        <div className="actions">
                            <Short>
                                {(i.data || !i.items.length) && <Icon i="delete" unBubble onClick={() => handleRemove(i)} />}
                                <Icon i="edit" unBubble onClick={() => setIsEdit(true)} />
                                {!i.data && <Icon i="add" unBubble onClick={() => handleAdd(i)} />}
                                {!i.data && <Icon i="link" unBubble onClick={() => handleBind(i)} />}
                                {isBind && <SearchBlock isExist={isExist} target={childRef} onClose={handleBindClose} onSelect={(data) => handleBindAdd(i, data)} />}
                            </Short>
                        </div>
                    )}
                </Short>
            )}
        </div>
    )
}
