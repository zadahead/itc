import './Absolute.css';

export const Absolute = ({ elem, children, stretch }) => {
    return (
        <div className='Absolute' data-stretch={stretch}>
            <div className='elem'>
                {elem}
            </div>
            <div className='content'>
                {children}
            </div>
        </div>
    )
}

export const Sticky = ({ children }) => {
    return (
        <div className='Sticky'>
            {children}
        </div>
    )
}