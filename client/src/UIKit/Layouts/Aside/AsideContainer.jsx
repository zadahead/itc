import { Tree } from 'UIKit';
import './AsideContainer.css';

export const AsideContainer = ({ children, side }) => {
    return (
        <div className='AsideContainer'>
            <aside>
                {side}
            </aside>
            <div>
                {children}
            </div>
        </div>
    )
}
