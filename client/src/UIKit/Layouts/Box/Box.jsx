import { forwardRef } from 'react';
import { Rows } from '../Line/Line';
import './Box.css';

export const Box = forwardRef(({ children, addClass, width }, ref) => {
    const styleCss = {
        width
    }

    return (
        <div ref={ref} className={`Box ${addClass || ''}`} style={styleCss}>
            {children}
        </div>
    )
})

export const Container = ({ title, info, children }) => {
    return (
        <Box>
            <div className='Container'>
                <Rows>
                    {title && <h4>{title}</h4>}
                    {info && <h5>{info}</h5>}
                    {children}
                </Rows>
            </div>
        </Box>
    )
}