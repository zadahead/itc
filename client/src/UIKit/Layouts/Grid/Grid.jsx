import './Grid.css';

export const Grid = ({ children, layout }) => {
    const list = children.length ? children : [children];
    return (
        <div className='Grid' layout={layout}>
            {list.map((c, i) => (
                <div key={i}>{c}</div>
            ))}
        </div>
    )
}

export const GridTopAuto = ({ children }) => {
    return (
        <div className='Grid top auto'>
            {children.map((c, i) => (
                <div key={i}>{c}</div>
            ))}
        </div>
    )
}