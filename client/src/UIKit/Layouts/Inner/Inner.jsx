import './Inner.css';

export const Inner = ({ children, addClass }) => {
    return (
        <div className={`Inner ${addClass || ''}`}>
            {children}
        </div>
    )
}

export const InnerSide = (props) => {
    return <Inner {...props} addClass="side" />
}
