import './Line.css';

export const Line = ({ children, addClass }) => {
    return (
        <div className={`Line ${addClass || ''}`}>
            {children}
        </div>
    )
}

export const Rows = (props) => {
    return <Line {...props} addClass="rows" />
}

export const RowsShort = (props) => {
    return <Line {...props} addClass="rows short" />
}

export const Between = (props) => {
    return <Line {...props} addClass="between" />
}

export const End = (props) => {
    return <Line {...props} addClass="end" />
}

export const Center = (props) => {
    return <Line {...props} addClass="center" />
}

export const CenterAlign = (props) => {
    return <Line {...props} addClass="center-align" />
}

export const Spread = (props) => {
    return <Line {...props} addClass="spread" />
}

export const SpreadEnd = (props) => {
    return <Line {...props} addClass="spread-end" />
}

export const Short = (props) => {
    return <Line {...props} addClass="short" />
}

export const SelfStart = (props) => {
    return <Line {...props} addClass="self-start" />
}

export const NoWrap = (props) => {
    return <Line {...props} addClass="no-wrap" />
}