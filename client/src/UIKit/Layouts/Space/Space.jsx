import './Space.css';

export const Space = ({ children, className }) => {
    return (
        <div className={className}>
            {children}
        </div>
    )
} 

export const Padding = (props) => {
    return <Space {...props} className="Padding" />
} 

export const Margin = (props) => {
    return <Space {...props} className="Margin" />
} 

export const MarginH = (props) => {
    return <Space {...props} className="Margin-h" />
} 