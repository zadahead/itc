import axios from "axios";
import { insertAtCursor, removeAtCursor } from "Helpers/dom";
import { useRef } from "react";
import { useEffect } from "react";
import { useState } from "react"
import { Markdown, Tabs, TextBox } from "UIKit"

export const MarkdownEditor = ({ markdown, onChange }) => {
    const [selected, setSelected] = useState();
    const [value, setValue] = useState('');

    const inputRef = useRef();

    useEffect(() => {
        document.addEventListener('paste', handleImagePaste);

        return () => {
            document.removeEventListener('paste', handleImagePaste);
        }
    }, [])

    useEffect(() => {
        handleSetValue(markdown);
    }, [markdown])

    const handleImagePaste = async (pasteEvent) => {
        if (document.activeElement !== inputRef.current) { return; }

        const LOADING = 'uploading...';


        // consider the first item (can be easily extended for multiple items)
        var item = pasteEvent.clipboardData.items[0];

        if (item.type.indexOf("image") === 0) {
            insertAtCursor(inputRef.current, LOADING);

            var blob = item.getAsFile();

            const formData = new FormData();
            formData.append('image', blob);

            const response = await axios({
                method: 'post',
                url: 'http://localhost:6500/upload',
                data: formData,
                headers: {
                    'Content-Type': `multipart/form-data`,
                },
            });

            removeAtCursor(inputRef.current, LOADING);

            insertAtCursor(inputRef.current, `![](${response.data.path})`);

            handleSetValue(inputRef.current.value);
            console.log(response);

        }
    }

    const handleSetValue = (value) => {
        setValue(value);
        onChange(value);
    }

    return (
        <Tabs
            selected={selected}
            onChange={setSelected}
            list={[
                {
                    icon: 'visibility',
                    text: 'Actual View',
                    content: <Markdown markdown={value} />
                },
                {
                    icon: 'edit',
                    text: 'Edit View',
                    content: <TextBox ref={inputRef} onChange={handleSetValue} value={value} />
                }
            ]}
        />
    )
}