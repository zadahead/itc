import { useNavigate, useParams } from "react-router-dom";
import { AsideContainer, Between, Btn, InnerSide, Line, Rows, Tree, Absolute, Grid, SelfStart, NoWrap, Sticky, Center, Margin, Tags, MarginH } from "UIKit";
import { MarkdownEditor } from "UIKit";
import { useEffect } from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadSubjects, setSubjects } from "State/docs";
import { axiosDelete, axiosGet, axiosPatch, axiosPost } from "State/axios";
import { useRef } from "react";
import { SideView } from "Components/SideView";
import { getNextSort } from "Helpers/high-order";
import { usePrompt } from "Hooks/usePrompt";

const findItem = (id, node) => {
    for (let i = 0; i < node.length; i++) {
        const item = node[i];
        if (!id && item.data) {
            return item;
        }

        if (item.id === id) {
            return item;
        }
        if (item.items) {
            const inner = findItem(id, item.items);
            if (inner) {
                return inner;
            }
        }
    }
}

const findSelectedOrFirst = (id, node) => {
    return findItem(id, node);
}

export const Doc = () => {
    const dispatch = useDispatch();
    const { subjects } = useSelector(state => state.docs);
    const { field, program, subject, selected } = useParams();
    const navigate = useNavigate();
    const [markdown, setMarkdown] = useState('');
    const [doc, setDoc] = useState(null);
    const [docIndex, setDocIndex] = useState(0);
    const [isNeedSave, setIsNeedSave] = useState(false);

    usePrompt("Document is not saved, continue?", isNeedSave);

    const docRef = useRef();
    docRef.current = doc;
    const { items = [] } = subjects;

    const item = findSelectedOrFirst(selected, items);

    useEffect(() => {
        const url = '/docs/' + [field, program, subject].join('/');
        dispatch(loadSubjects(url));
    }, [])

    useEffect(() => {
        if (item && item.data && item.data !== 'new') {
            if (!selected) {
                handleSelected(item);
            }
            axiosGet(`/doc/${item.data}`).then(resp => {
                setDocIndex(0);
                setDoc(resp);
            });
            setIsNeedSave(false);
        }
    }, [item, selected])

    useEffect(() => {
        if (!doc || !doc.docs || !doc.docs[docIndex]) { return }

        const content = doc.docs[docIndex].content;
        setMarkdown(content || '# Content');
    }, [docIndex, doc])

    const handleChange = (list) => {
        dispatch(setSubjects([...list]))
    }

    const handleSelected = async (item) => {

        if (!item) {
            return navigate(`/docs/${field}/${program}/${subject}`);
        }

        navigate(`/docs/${field}/${program}/${subject}/${item.id}`);

    }

    const handleCancel = () => {
        console.log('handleCancel');
    }

    const handleAdd = async (item, parent, bind) => {
        if (bind) {
            const insertedId = await axiosPost(`/docs/tree/${parent}`, bind);
            item.id = insertedId;
            item.data = bind.data;
        } else {
            const resp = await axiosPost('/doc', {
                title: item.title,
                parent: parent || subjects.id,
                isWithData: !!parent,
                tags: [subjects.title],
                sort: item.sort
            })
            item.id = resp.itemId;
            item.data = resp.docId;
        }

        handleSelected(item);
    }

    const handleItemChange = async (item) => {
        await axiosPatch(`/docs/${item.id}`, { title: item.title });
    }

    const handleItemDelete = async (item) => {
        await axiosDelete(`/docs/${item.id}`);
        handleSelected();
    }

    const handleSaveDoc = async () => {
        const ref = docRef.current;
        const d = ref.docs[docIndex];

        const main = {
            title: ref.docs[0].title,
            tags: ref.tags
        }

        await axiosPatch(`/doc/${doc.id}`, { ...d, main });

        saveDoc();
        setIsNeedSave(false);
    }

    const handleChangeDoc = async (value) => {
        const ref = docRef.current;
        if (!ref || !ref.docs || !ref.docs[docIndex]) { return }

        const d = ref.docs[docIndex];

        if (d.content !== value) {
            d.content = value;

            const title = value.replace('\n', '*').match(/\# ([A-Za-z0-9\s$]*)/);

            if (title) {
                d.title = title[1];
            }

            setIsNeedSave(true);
        }

    }

    const handleAddArticle = async () => {
        const ref = docRef.current;
        if (!ref || !ref.docs || !ref.docs[docIndex]) { return }

        const article = {
            content: '# New Article',
            title: 'New Article'
        }

        const newId = await axiosPost(`/doc/${doc.id}`, article);

        article.id = newId;
        article.sort = getNextSort(ref.docs);

        ref.docs.push(article);

        saveDoc();
        setDocIndex(ref.docs.length - 1);
    }

    const handleSelectArticle = (index) => {
        setDocIndex(index)
    }

    const handleDeleteArticle = async (index) => {
        const approve = await window.conf("This will remove article completely, continue?");

        if (approve) {
            const ref = docRef.current;
            if (!ref || !ref.docs || !ref.docs[index]) { return }

            const d = ref.docs[index];

            await axiosDelete(`/doc/${doc.id}/${d.id}`);

            ref.docs.splice(index, 1);

            saveDoc();
            setDocIndex(0)
        }
    }

    const handleSortArticle = async (list) => {
        const ref = docRef.current;
        if (!ref || !ref.docs || !ref.docs[docIndex]) { return }

        const req = {
            parent: ref.id,
            items: list.map(i => i.id)
        }

        await axiosPatch('/doc/sort', req);

        saveDoc();

    }

    const handleTagDoc = (tags) => {
        const ref = docRef.current;
        if (!ref || !ref.docs || !ref.docs[docIndex]) { return }

        ref.tags = tags;

        saveDoc();
    }

    const saveDoc = () => {
        const ref = docRef.current;
        if (!ref || !ref.docs || !ref.docs[docIndex]) { return }

        setDoc({ ...ref });
        setIsNeedSave(true);
    }
    const handleSort = async (sParent, tParent) => {
        const req = (obj) => ({
            parent: obj.id,
            items: obj.items.map(i => i.id)
        })

        await axiosPatch('/docs/sort', req(sParent));
        if (tParent.id !== sParent.id) {
            await axiosPatch('/docs/sort', req(tParent));
        }
    }



    return (
        <div>
            <AsideContainer
                side={
                    <Rows>
                        <Tree
                            parent={subjects}
                            selected={selected}
                            onAdd={handleAdd}
                            onChange={handleChange}
                            onselect={handleSelected}
                            onCancel={handleCancel}
                            onItemChange={handleItemChange}
                            onItemDelete={handleItemDelete}
                            onSort={handleSort}
                        />
                    </Rows>
                }
            >
                <NoWrap>
                    {item ? (
                        <>
                            <InnerSide>
                                <Between>
                                    <h4>{subjects.path}</h4>
                                    <Line>
                                        <Btn onClick={handleSaveDoc} disabled={!isNeedSave}>Save</Btn>
                                    </Line>
                                </Between>
                                <MarginH>
                                    <Rows>
                                        <h3>Tags</h3>
                                        <Tags list={doc?.tags} onChange={handleTagDoc} />
                                    </Rows>
                                </MarginH>
                                <MarkdownEditor markdown={markdown} onChange={handleChangeDoc} />
                            </InnerSide>
                            <Sticky>
                                <SideView
                                    title="In This Article"
                                    list={doc?.docs}
                                    selected={docIndex}
                                    onAdd={handleAddArticle}
                                    onSelect={handleSelectArticle}
                                    onDelete={handleDeleteArticle}
                                    onSort={handleSortArticle}
                                />
                            </Sticky>
                        </>
                    ) : (
                        <Center>
                            <h2>No Content</h2>
                        </Center>
                    )}
                </NoWrap>
            </AsideContainer>
        </div>
    )
}