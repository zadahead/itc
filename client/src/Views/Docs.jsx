import { Center, Grid, Inner, CubeIcon, Rows, Margin, Line, Btn, Icon, Between } from "UIKit";
import ReactLogo from 'Images/logos/react.png';
import HTMLLogo from 'Images/logos/html.png';
import JSLogo from 'Images/logos/js.png';
import NodeLogo from 'Images/logos/nodejs.png';
import MongoLogo from 'Images/logos/mongo.png';
import MySQLLogo from 'Images/logos/mysql.png';
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { loadSubjects, loadTabs } from "State/docs";
import { useLocation, useParams } from "react-router-dom";
import { axiosDelete, axiosPatch, axiosPost } from "State/axios";
import { useState } from "react";
import { getNextSort } from "Helpers/high-order";

export const Docs = () => {
    const dispatch = useDispatch();
    const [onEdit, setOnEdit] = useState(null);
    const { subjects } = useSelector(state => state.docs);
    const { pathname } = useLocation();

    const { items = [] } = subjects;

    useEffect(() => {
        dispatch(loadSubjects(pathname));
    }, [pathname])

    const handleAdd = async () => {
        const sort = getNextSort(subjects.items);
        await axiosPost(`/docs/tree/${subjects.id}`, { title: "New Subject", sort });
        dispatch(loadSubjects(pathname));
    }

    const handleDelete = async (item) => {
        await axiosDelete(`/docs/${item.id}`);
        dispatch(loadSubjects(pathname));
    }

    const handleChange = async (item, title) => {
        await axiosPatch(`/docs/${item.id}`, { title })
        dispatch(loadSubjects(pathname));
        setOnEdit(null);
    }

    const handleSortLeft = async (item) => saveSort(item, -1);
    const handleSortRight = async (item) => saveSort(item, 1);

    const saveSort = async (item, prefix) => {
        const list = subjects.items;
        const sIndex = list.findIndex(i => i.id === item.id);

        list.splice(sIndex + prefix, 0, list.splice(sIndex, 1)[0]);

        const req = {
            parent: subjects.id,
            items: list.map(i => i.id)
        }

        await axiosPatch('/docs/sort', req);
        dispatch(loadSubjects(pathname));
    }

    return (
        <Center>
            <Inner>
                <Rows>
                    <Center>
                        <Margin>
                            <Line>
                                <h1>{subjects.title} List</h1>
                            </Line>
                        </Margin>
                    </Center>
                    <Grid layout='cubes'>
                        {[
                            ...items.map((i, index) => (
                                <CubeIcon
                                    key={i.id}
                                    text={i.title}
                                    to={`/docs${i.path}`}
                                    isEdit={onEdit === i.id}
                                    onChange={(text) => handleChange(i, text)}
                                    actions={(
                                        <Between>
                                            <Line>
                                                {index > 0 && <Icon i="keyboard_double_arrow_left" unBubble onClick={() => handleSortLeft(i)} />}
                                                {(index < items.length - 1) && <Icon i="keyboard_double_arrow_right" unBubble onClick={() => handleSortRight(i)} />}
                                            </Line>
                                            <Line>
                                                <Icon i="edit" unBubble onClick={() => setOnEdit(i.id)} />
                                                {!i.items.length && <Icon i="delete" unBubble onClick={() => handleDelete(i)} />}
                                            </Line>
                                        </Between>
                                    )}
                                />
                            )),
                            <Center key={'btn'}>
                                <Btn onClick={handleAdd}>Add New</Btn>
                            </Center>
                        ]}

                    </Grid>
                </Rows>
            </Inner>
        </Center>
    )
}

/*
<CubeIcon src={ReactLogo} text="React JS" to='/docs/react' />
                        <CubeIcon src={HTMLLogo} text="HTML/Css" to='/docs/html' />
                        <CubeIcon src={JSLogo} text="Javascript" to='/docs/javascript' />
                        <CubeIcon src={NodeLogo} text="Node JS" to='/docs/nodejs' />
                        <CubeIcon src={MongoLogo} text="Mongo DB" to='/docs/mongodb' />
                        <CubeIcon src={MySQLLogo} text="MySQL" to='/docs/mysql' />
*/