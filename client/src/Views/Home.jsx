import { Center } from "UIKit";

export const Home = () => {
    return (
        <Center>
            <h2>ITC - Israel Tech Challenge</h2>
        </Center>
    )
}