import { useEffect } from "react"
import { useDispatch } from "react-redux"
import { AddSet } from "../Components/AddSet"
import { Jobs } from "../Components/Jobs"
import { Subjects } from "../Components/Subjects"
import { getJobs } from "../State/jobs"
import { Grid } from "UIKit"

export const JobSeek = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(getJobs);
    }, [dispatch])

    return (
        <Grid layout="10,fr,30">
            <Jobs />
            <AddSet />
            <Subjects />
        </Grid>
    )
}