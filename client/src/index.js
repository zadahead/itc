import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router } from 'react-router-dom';

import './index.css';
import { App } from './App';

import Store from "./State/store";
import { DialogProvider } from 'Context/dialogContext';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Store>
    <DialogProvider>
      <Router>
        <App />
      </Router>
    </DialogProvider>
  </Store>
);


