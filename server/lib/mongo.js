const connectToDatabase = require("./conn");


const handler = async () => {

    const { database } = await connectToDatabase();

    return {
        jobseek: database.collection('jobseek'),
        docs: database.collection('docs'),
        docsTree: database.collection('docs_tree')
    }
}

module.exports = handler;