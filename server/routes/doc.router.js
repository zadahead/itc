const express = require('express');
const { ObjectId } = require('mongodb');
const router = express.Router();
const db = require('../lib/mongo');
const { addChild } = require('../services/docs.service');

router.post('/search', async (req, res) => {
    const { docs } = await db();
    const { val } = req.body;

    const resp = await docs.find(
        {
            $or: [
                {
                    title: { $regex: new RegExp(val, 'i') }
                },
                {
                    tags: {
                        $elemMatch: { $regex: new RegExp(val, 'i') }
                    }
                },
                {
                    docs: {
                        $elemMatch: {
                            title: { $regex: new RegExp(val, 'i') }
                        }
                    }
                }
            ]
        },
        { title: 1 }
    ).limit(5).toArray();

    const map = resp.map(i => {
        return {
            id: i._id,
            title: i.title,
            docs: i.docs.map(d => ({ id:d.id, title: d.title})),
            tags: i.tags
        }
    })

    res.send(map);
})

router.get('/:id', async (req, res) => {
    const { docs } = await db();
    const { id } = req.params;

    const resp = await docs.findOne({ _id: ObjectId(id) })
    if(resp?.docs) {
        resp.docs.sort((a, b) => {
            return a.sort > b.sort ? 1 : -1;
        })
    }
   

    res.send({ ...resp, id: resp?._id });
})

router.post('/', async (req, res) => {
    const { title, parent, isWithData, tags, sort } = req.body;
    const { docs } = await db();

    if (isWithData) {
        const newDoc = await docs.insertOne({
            title,
            tags,
            docs: [
                { id: ObjectId(), title, content: `# ${title}` }
            ]
        });

        const insertedId = await addChild({ title, sort, data: newDoc.insertedId }, parent);

        res.send({
            itemId: insertedId,
            docId: newDoc.insertedId
        });
    } else {
        const insertedId = await addChild({ title, sort }, parent);

        res.send({
            itemId: insertedId
        });
    }

})


router.patch('/sort', async (req, res) => {
    const { docs } = await db();

    const { parent, items } = req.body;

    const par = await docs.findOne({ _id: ObjectId(parent) });

    const map = items.reduce((state, item, index) => {
        return { ...state, [item]: index };
    }, {})


    par.docs.forEach(item => {
        item.sort = map[item.id];
    });


    console.log(par);

    await docs.updateOne(
        { _id: ObjectId(parent) },
        {
            $set: { docs: par.docs }
        }
    )

    res.send('success');
})



router.patch('/:docId', async (req, res) => {
    const { docId } = req.params;
    const { id, content, title, sort, main } = req.body;

    const { docs } = await db();

    const resp = await docs.updateOne(
        { _id: ObjectId(docId), "docs.id": ObjectId(id) },
        {
            $set: {
                "title": main.title,
                "tags": main.tags,
                "docs.$.content": content,
                "docs.$.title": title,
                "docs.$.sort": sort
            }
        }
    )

    res.send(resp);
})

router.post('/:docId', async (req, res) => {
    const { docId } = req.params;
    const { content, title } = req.body;

    const newId = ObjectId();

    const { docs } = await db();

    await docs.updateOne(
        { _id: ObjectId(docId) },
        {
            $push: {
                "docs": {
                    content,
                    title,
                    id: newId
                }
            }
        }
    )

    res.send(newId);
})

router.delete('/:docId/:articleId', async (req, res) => {
    const { docId, articleId } = req.params;

    const { docs } = await db();

    const resp = await docs.updateOne(
        { _id: ObjectId(docId) },
        {
            $pull: {
                "docs": {
                    id: ObjectId(articleId)
                }
            }
        }
    )

    res.send(resp);
})



module.exports = router;