const express = require('express');
const { ObjectId } = require('mongodb');
const router = express.Router();
const db = require('../lib/mongo');
const { deleteIfNoBinds } = require('../services/doc.service');
const { addChild, toUrl, changePath, getTree, bindChildren } = require('../services/docs.service');

router.get('/', async (req, res) => {
    const { docsTree } = await db();

    const parent = await docsTree.findOne({ path: '/' });

    const resp = await docsTree.find({ parent: ObjectId(parent._id) }).toArray();

    const map = resp.map(i => (
        {
            id: i._id,
            title: i.title,
            url: i.url
        }
    ))
    res.send(map);
})

router.get('/parent/:id', async (req, res) => {
    const { docsTree } = await db();
    const { id } = req.params;

    const resp = await docsTree.find({ parent: ObjectId(id) }).toArray();


    res.send(resp);
})

router.delete('/:id', async (req, res) => {
    const { docsTree } = await db();
    const { id } = req.params;

    const item = await docsTree.findOne({ _id: ObjectId(id) });

    const resp = await docsTree.deleteOne({ _id: ObjectId(id) });

    await deleteIfNoBinds(item.data);

    res.send(resp);
})

router.get('/tree', async (req, res) => {
    const { docsTree } = await db();

    const resp = await docsTree.find({}).toArray();
    res.send(resp);
})

router.get('/path/:parent', async (req, res) => {
    const { docsTree } = await db();
    const { parent } = req.params;
    const resp = await docsTree.find({
        $or: [
            { path: ObjectId(parent) },
            { _id: ObjectId(parent) }
        ]
    }).toArray();

    const map = resp.reduce((state, it) => {
        const item = {
            id: it._id,
            title: it.title,
            parent: it.path[it.path.length - 1],
            items: [],
            sort: it.sort
        }
        return { ...state, [it._id]: item }
    }, {})

    Object.keys(map).forEach(key => {
        const item = map[key];
        const { parent } = item;
        if (parent && map[parent]) {
            map[parent].items.push(item);
            console.log(key);
        }
    })

    res.send(map[parent]);
})

router.post('/tree/:parent', async (req, res) => {
    const { parent } = req.params;
    const insertedId = await addChild(req.body, parent);

    res.send(insertedId);
})

router.post('/set/url', async (req, res) => {
    const { docsTree } = await db();

    const tree = await docsTree.find({}).toArray();

    tree.forEach(item => {
        item.url = item.title
            .toLowerCase()
            .replace('?', '')
            .replace('=', '')
            .replace('&', '')
            .split(' ')
            .join('-');
    })

    await docsTree.remove();

    const resp = await docsTree.insert(tree);
    res.send(resp);
})

router.patch('/sort', async (req, res) => {
    const { docsTree } = await db();

    const { parent, items } = req.body;

    const par = await docsTree.findOne({ _id: ObjectId(parent) });

    for (let i = 0; i < items.length; i++) {
        const id = items[i];
        const item = await docsTree.findOne({ _id: ObjectId(id)});
        const url = toUrl(item.title);

        await docsTree.updateOne(
            {
                _id: ObjectId(id)
            },
            {
                $set: {
                    sort: i,
                    path: [par.path, url].join('/'),
                    parent: parent
                }
            }
        )
    }

    res.send('success');
})

router.get('/*', async (req, res) => {
    const { url } = req;
    const tree = await getTree(url);

    tree.items.sort((a, b) => {
        return a.sort > b.sort ? 1 : -1;
    })
    res.send(tree);
})


router.patch('/:id', async (req, res) => {
    const { docsTree } = await db();
    const { title } = req.body;
    const { id } = req.params;

    const item = await docsTree.findOne({ _id: ObjectId(id) });

    const children = await getTree(item.path);

    children.title = title;
    const url = toUrl(title);
    const path = changePath(item.path, url);
    
    await bindChildren(children, path);

    res.send('success');
})


module.exports = router;