const express = require('express');
const router = express.Router();
const db = require('../lib/mongo');

router.get('/', async (req, res) => {
    const { jobseek } = await db();
    const list = await jobseek.findOne({});
    res.send(list);
})

router.post('/save', async (req, res) => {
    const { jobseek } = await db();
    await jobseek.remove();

    const resp = await jobseek.insert(req.body);
    res.send(resp);
})

module.exports = router;