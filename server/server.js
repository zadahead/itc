const express = require('express');
const cors = require('cors');

const app = express();

app.use(cors({
    origin: '*'
}))

app.use(express.json());

app.use('/jobseek', require('./routes/jobseek.router'));
app.use('/docs', require('./routes/docs.router'));
app.use('/doc', require('./routes/doc.router'));

const PORT = process.env.PORT;

app.listen(PORT, () => {
    console.log(`server is running (:${PORT})`);
})


