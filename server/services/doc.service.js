const { ObjectId } = require('mongodb');
const db = require('../lib/mongo');
const { getBindedDocs } = require('./docs.service');

module.exports.deleteIfNoBinds = async (docId) => {
    const { docs } = await db();
    const length = await getBindedDocs(docId);

    if(!length) {
        await docs.deleteOne( { _id: ObjectId(docId )});
    }
}