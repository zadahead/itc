const { ObjectId } = require('mongodb');
const db = require('../lib/mongo');

module.exports.addChild = async (body, parent) => {
    const { title, data, sort } = body;

    const { docsTree } = await db();

    const parentId = ObjectId(parent);

    const item = await docsTree.findOne({ _id: parentId });

    const url = this.toUrl(title);

    const resp = await docsTree.insertOne(
        {
            _id: ObjectId(),
            title,
            data: data ? ObjectId(data) : null,
            path: this.toPath(item.path, url),
            parent: parentId,
            sort,
            url: url
        }
    );

    return resp.insertedId;
}

module.exports.getTree = async (url) => {
    const { docsTree } = await db();

    const resp = await docsTree.find({
        path: { $regex: url }
    }).toArray();

    const map = resp.reduce((state, it) => {
        const item = {
            id: it._id,
            title: it.title,
            parent: it.parent,
            path: it.path,
            data: it.data,
            sort: it.sort,
            items: []
        }
        return { ...state, [it._id]: item }
    }, {})

    Object.keys(map).forEach(key => {
        const item = map[key];
        const { parent } = item;
        if (parent && map[parent]) {
            map[parent].items.push(item);
            map[parent].items.sort((a, b) => {
                if (!a.data) { return 1 }
                return a.sort > b.sort ? 1 : -1;
            });
        }
    })

    const id = Object.keys(map).find(key => map[key].path === url);
    return map[id];
}

module.exports.getBindedDocs = async (docId) => {
    const { docsTree } = await db();

    const item = await docsTree.find({ data: (docId) }).toArray();
    console.log(item);

    return item.length;
}

module.exports.toUrl = (str) => {
    return str.toLowerCase()
        .replace('?', '')
        .replace('=', '')
        .replace('&', '')
        .replace('/', '-')
        .replace('\\', '-')
        .split(' ')
        .join('-');
}

module.exports.changePath = (path, url) => {
    const splt = path.split('/');
    splt[splt.length - 1] = url;

    return splt.join('/');
}

module.exports.toPath = (path, url) => [path, url].join('/');

module.exports.bindChildren = async (item, path, isAppend) => {
    const { docsTree } = await db();

    const url = this.toUrl(item.title);
    const p = isAppend ? this.toPath(path, url) : this.changePath(path, url);

    await docsTree.updateOne(
        {
            _id: ObjectId(item.id)
        },
        {
            $set: {
                title: item.title,
                url,
                path: p
            }
        }
    )

    console.log(url, p);

    for (let i = 0; i < item.items.length; i++) {
        const it = item.items[i];

        await this.bindChildren(it, p, true);
    }
}