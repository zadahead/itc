require('dotenv').config();
const multer = require('multer');
const sharp = require('sharp');
const cors = require('cors');

const express = require('express');
const app = express();

app.use(cors({
    origin: '*'
}))

app.use(express.static('assets'));

const filter = (req, file, cb) => {
    if (file.mimetype.split("/")[0] === 'image') {
        cb(null, true);
    } else {
        cb(new Error("Only images are allowed!"));
    }
};

const storage = multer.memoryStorage();

const upload = multer({ storage: storage, fileFilter: filter })

app.post('/upload', upload.single('image'), async (req, res) => {
    // req.file is the name of your file in the form above, here 'uploaded_file'
    // req.body will hold the text fields, if there were any 
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9)
    const root = `./assets`;
    const path = `/${uniqueSuffix}-${req.file.originalname}`;

    // toFile() method stores the image on disk
    await sharp(req.file.buffer).resize(700, 700, { fit: 'outside' }).toFile(root + path);

    res.send({ path });
});

const { PORT } = process.env;

app.listen(PORT, () => {
    console.log(`file server in listening in port:${PORT}`);
})